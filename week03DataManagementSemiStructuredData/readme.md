## LOG ANALYSIS EXERCISE

### This exercise consists of 4 parts:

Part 1: Apache Web Server Log file format

Part 2: Sample Analyses on the Web Server Log File

Part 3: Analyzing Web Server Log File

Part 4: Exploring 404 Response Codes

- [lab02 Web Server Log Analysis with Apache Spark](http://nbviewer.ipython.org/github/spark-mooc/mooc-setup/blob/master/lab2_apache_log_student.ipynb)

