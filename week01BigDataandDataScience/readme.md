### This notebook covers: 

Part 1: Test Spark functionality

Part 2: Check class testing library

Part 3: Check plotting

Part 4: Check MathJax formulas

Part 5: Export / download and submit

- [lab00 First Notebook: Virtual machine test and assignment submission](http://nbviewer.ipython.org/github/spark-mooc/mooc-setup/blob/master/lab0_student.ipynb)