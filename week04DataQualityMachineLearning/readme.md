## TEXT ANALYSIS AND ENTITY RESOLUTION EXERCISE

### This exercise consists of 5 parts and quiz questions:

Part 1: ER as Text Similarity - Bags of Words

Part 2: ER as Text Similarity - Weighted Bag-of-Words using TF-IDF

Part 3: ER as Text Similarity - Cosine Similarity

Part 4: Scalable ER

Part 5: Analysis (this is part where you will click through and view plots of your work from part 4)



- [lab03 Text Analysis and Entity Resolution](http://nbviewer.ipython.org/github/spark-mooc/mooc-setup/blob/master/lab3_text_analysis_and_entity_resolution_student.ipynb)
