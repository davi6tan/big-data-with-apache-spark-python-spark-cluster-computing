## INTRODUCTION TO MACHINE LEARNING WITH APACHE SPARK

### This exercise consists of 3 parts and quiz questions:

Part 1: Basic Recommendations

Part 2: Collaborative Filtering

Part 3: Predictions for Yourself (this is part where you will enter your own ratings and see what movies are recommended for you)

- [lab4 Introduction to Machine Learning with Apache Spark](http://nbviewer.ipython.org/github/spark-mooc/mooc-setup/blob/master/lab4_machine_learning_student.ipynb)