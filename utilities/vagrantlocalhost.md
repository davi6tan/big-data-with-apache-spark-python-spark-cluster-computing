### VIRTUALBOX FOR MAC OS X

- [steps](https://youtu.be/3atHHNa2UwI)

- [downloads site](https://www.virtualbox.org/wiki/Downloads)


### Downloading and Installing Vagrant

- [downloads site](https://www.vagrantup.com/downloads.html)

- [steps](https://youtu.be/hPmawNk2XQw)



### go to folder
Davids-MacBook-Pro:myvagrant davidtan$ ls
Vagrantfile		mooc-setup-master
 
1) `to run  vagrant up`

- [http://localhost:8001/](http://localhost:8001/)

2) `to stop  vagrant halt`
